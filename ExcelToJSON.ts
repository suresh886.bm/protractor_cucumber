import * as fs from "fs";
import * as excel from "xlsx";

interface ICONFIG {
    [key: string]: string;
}
interface ICUSTOMJSON {
    [key: string]: ICONFIG[];
}
interface ICREDENTIALS {
    reviewerUsername: string;
    reviewerPassword: string;
    approverUsername: string;
    approverPassword: string;
    sysAdminUsername: string;
    sysAdminPassword: string;
}

interface ICOMPANYDETAILS {
    applicationURL: string;
    mailCatcherURL: string;
    companyName: string;
    companyGLN: string;
    companyName2: string;
    companyGLN2: string;
}

interface ITIMEOUTS {
    pageLoadTimeout: number;
    implicitWaitTime: number;
    explictWaitTime: number;
    smallSleepTimeout: number;
}
interface IVERIFICATIONSCOPE {
    tableVerificationScope: string;
}

interface IVERIFICATIONSCOPE {
    tableVerificationScope: string;
}

interface IVERIFICATIONSCOPE {
    tableVerificationScope: string;
    tableVerificationScopeForGLN: string;
    maxNoOfPagesToVerifyInTable: string;
}

export declare let testConfig: ICUSTOMJSON;
export declare let credentials: ICREDENTIALS;
export declare let companayDetails: ICOMPANYDETAILS;
export declare let timeouts: ITIMEOUTS;
export declare let verificationScope: IVERIFICATIONSCOPE;

export function isTestSuiteEnabled (testCaseSettings: ICONFIG[], testSuiteID: string): boolean
{
    const testSuite = testCaseSettings.find (item => item.TestSuiteID === testSuiteID);
    return testSuite ? testSuite.ExecutionFlag.toUpperCase () === "YES" : false;

}

export function getConfigurationValue (keyword: string): string
{
    const configuration = testConfig.Configuration.find (item => item.Keyword === keyword);
    return configuration ? configuration.Value : "null";
}

export function getGtinValue (testCaseSettings: ICONFIG[], testSuiteID: string): string
{
    const testSuite = testCaseSettings.find (item => item.TestSuiteID === testSuiteID);
    return testSuite ? testSuite.GTIN : "";

}

export class ExcelJSON
{        
    public convertToJSON (inputFileName: string, outputFileName: string): void
    {
        const workBook: excel.WorkBook = excel.readFile (inputFileName);
        const jsonData: ICUSTOMJSON = {};
        workBook.SheetNames.forEach ((sheetName) =>
        {
            jsonData[sheetName] = excel.utils.sheet_to_json (workBook.Sheets[sheetName], {raw: true, range: "A1:KD500", header: 0});
        });
        // console.log('JSON',JSON.stringify(jsonData,null,4));
        const space = 4;
        fs.writeFileSync (outputFileName, JSON.stringify (jsonData, null, space));
    }

    public readJSONFile (jsonFileName: string): ICUSTOMJSON
    {
        const json = fs.readFileSync (jsonFileName);
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return JSON.parse (json.toString ()); 
    }

    public readTestConfiguration (inputFileName: string, outputFileName: string): void
    {
        const workBook: excel.WorkBook = excel.readFile (inputFileName);
        // const jsonData = new Map<string, unknown[]> ();
        testConfig = {};
        workBook.SheetNames.forEach ((sheetName) =>
        {
            const sheetToJSON = excel.utils.sheet_to_json (workBook.Sheets[sheetName], {raw: true, range: "B2:G500", header: 0});
            // console.log (sheetToJSON);
            testConfig [sheetName] = sheetToJSON as unknown as ICONFIG[];            
        });
        /// console.log (Array.from (jsonData.entries ()));
        // console.log ('JSON', JSON.stringify (),null, 4));
        // const space = 4;
        // const obj = JSON.stringify (jsonData, null, space);
        // const objJson: CUSTOMEJSON = <CUSTOMEJSON>JSON.parse (obj);
        // console.log (objJson.Configuration);
        // fs.writeFileSync (outputFileName, JSON.stringify (testConfig, null, space));

        // updating the CREDENTIALS and COMPANYDETAILS to access across project
        credentials = {
            reviewerUsername: getConfigurationValue ("Reviewer_Username"),
            reviewerPassword: getConfigurationValue ("Reviewer_Password"),
            approverUsername: getConfigurationValue ("Approver_Username"),
            approverPassword: getConfigurationValue ("Approver_Password"),
            sysAdminUsername: getConfigurationValue ("SysAdmin_Username"),
            sysAdminPassword: getConfigurationValue ("SysAdmin_Password")
        };
        companayDetails = {
            applicationURL: getConfigurationValue ("Application_URL"),
            mailCatcherURL: getConfigurationValue ("MailCatcher_URL"),
            companyName: getConfigurationValue ("Company_Name"),
            companyGLN: getConfigurationValue ("GLN"),
            companyName2: getConfigurationValue ("Company_Name_2"),
            companyGLN2: getConfigurationValue ("GLN_2"),
        };
        timeouts = {
            pageLoadTimeout: Number (getConfigurationValue ("PageLoadTimeout")),
            implicitWaitTime: Number (getConfigurationValue ("ImplicitlyWaitTime")),
            explictWaitTime: Number (getConfigurationValue ("ExplicitWaitTime")),
            smallSleepTimeout: Number (getConfigurationValue ("SmallSleepTimeout")),
        };
        verificationScope = {
            tableVerificationScope : getConfigurationValue ("TableVerificationScope"),
            tableVerificationScopeForGLN : getConfigurationValue ("TableVerificationScopeForGLN"),
            maxNoOfPagesToVerifyInTable : getConfigurationValue ("MaxNoOfPagesToVerifyInTable")
            
        };
        
    }

}

new ExcelJSON ().readTestConfiguration ("./TestConfiguration_EcomNutrition.xlsx", "./testConfiguration.json");