var HTMLReport = require('protractor-html-reporter-2');
var jasmineReporters = require('jasmine-reporters');

exports.config = {

  directConnect: true,
  getPageTimeout: 60000,
  allScriptsTimeout: 50000,
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),

  capabilities: {
    'browserName': 'chrome'
  },

  specs: ['./features/*.feature'], // // accepts a globally

  cucumberOpts: {
    require: [
      './stepDefinitions/stepDefinition.js' // accepts a globally
    ],
    dryRun: true
  },
  
    
    


};